# OakLeaves

Personal neovim config similar to an IDE




https://github.com/LunarVim/LunarVim
https://github.com/orgs/LunarVim/repositories


Treesitter
LSP client
Formatter
telescope
Theprimeagen
Minimap
Lualine
Hop
Sneak
Vimux
Nvim tree
Git gutter
Nvim LSP
Polyglot
nerdtree
auto-pairs
lsp_signature
compe
compe-zsh
gitsign
nvim toggleterm
vim-floaterm
nvim-dap
vimspector


