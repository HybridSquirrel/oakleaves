

-- Use only at configuration 
require('plugins')

-- Use later when done, becomes faster load times
--require('plugin/packer_compiled')

require('config')
require('keybinds')
require('theme')


