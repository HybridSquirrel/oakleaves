[[File stucture]]




### Plugins that is installed:
* wbthomason/packer.nvim
https://github.com/wbthomason/packer.nvim
---
* neovim/nvim-lspconfig
https://github.com/neovim/nvim-lspconfig
---
* williamboman/nvim-lsp-installer
https://github.com/williamboman/nvim-lsp-installer
---
* nvim-telescope/telescope.nvim
https://github.com/nvim-telescope/telescope.nvim
---
* ahmedkhalf/project.nvim -- removed
https://github.com/ahmedkhalf/project.nvim
---
* hrsh7th/nvim-cmp
https://github.com/hrsh7th/nvim-cmp
---
* windwp/nvim-autopairs
https://github.com/windwp/nvim-autopairs
---
* nvim-treesitter/nvim-treesitter
https://github.com/nvim-treesitter/nvim-treesitter
---
* kyazdani42/nvim-web-devicons
https://github.com/kyazdani42/nvim-web-devicons
---
* shadmansaleh/lualine.nvim -- removed
https://github.com/shadmansaleh/lualine.nvim
---
* glepnir/dashboard-nvim
https://github.com/glepnir/dashboard-nvim
---
* akinsho/toggleterm.nvim
https://github.com/akinsho/toggleterm.nvim
---
* lewis6991/impatient.nvim
https://github.com/lewis6991/impatient.nvim
---
* rinx/nvim-minimap -- removed
https://github.com/rinx/nvim-minimap
---

antoinemadec/FixCursorHold.nvim
Needed while issue https://github.com/neovim/neovim/issues/12587 is still open

This will be implemented 
folke/which-key.nvim




