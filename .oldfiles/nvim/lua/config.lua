

-- //////////////////////////////////////////
--[[

Fix the fucking web DevIcons colors!!!!!!!!!

Remeber to clean up file and configure correctly 
everying the way you want not default

]]--
-- /////////////////////////////////////////


-- Config for Profiling
-- Plugins lewis6991/impatient
require'impatient'.enable_profile()


-- Config web-icons
-- Plugins nvim-web-devicons
require'nvim-web-devicons'.setup {
 -- your personnal icons can go here (to override)
 -- DevIcon will be appended to `name`
 override = {
  zsh = {
    icon = "",
    color = "#428850",
    name = "Zsh"
  }
 };
 -- globally enable default icons (default to false)
 -- will get overriden by `get_icons` option
 default = true;
}


-- Config LSPInstall
-- Plugins williamboman/nvim-lsp-install
local lsp_installer = require("nvim-lsp-installer")

lsp_installer.on_server_ready(function(server)
    local opts = {}

    -- (optional) Customize the options passed to the server
    -- if server.name == "tsserver" then
    --     opts.root_dir = function() ... end
    -- end

    -- This setup() function is exactly the same as lspconfig's setup function (:help lspconfig-quickstart)
    server:setup(opts)
    vim.cmd [[ do User LspAttachBuffers ]]
end)

require("nvim-lsp-installer").settings {
    ui = {
        icons = {
            server_installed = "✓",
            server_pending = "➜",
            server_uninstalled = "✗"
        }
    }
}


-- Config LSP
-- Plugins neovim/nvim-lspconfig
local nvim_lsp = require('lspconfig')

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
	local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

	-- Enable completion triggered by <c-x><c-o>
	buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
	
end
-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
--local servers = { 'bashls', 'clangd' }
--for _, lsp in ipairs(servers) do
--  nvim_lsp[lsp].setup {
--    on_attach = on_attach,
--    flags = {
--      debounce_text_changes = 150,
--    }
--  }
--end


-- Config autocomplete
-- Plugins hrsh7th/nvim-cmp | hrsh7th/cmp-buffer | hrsh7th/cmp-nvim-lsp
local cmp = require('cmp')
cmp.setup({
		
	sources = {
	{ name = 'nvim_lsp' },

	{ name = 'buffer' },
	}
})
-- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)


-- Config with autopairs
-- Plugins windwp/nvim-autopairs
require('nvim-autopairs').setup{}


-- Config telescope
-- Plugins nvim-telescope/telescope.nvim 
require('telescope').setup{
  defaults = {
	layout_config = {
	vertical = { width = 0.5 },
	},
  pickers = {
	find_files = {
	theme = "dropdown",
    }
  },
  extensions = {
	fzf = {
	fuzzy = true,                    -- false will only do exact matching
	override_generic_sorter = true,  -- override the generic sorter
	override_file_sorter = true,     -- override the file sorter
	case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
	                               -- the default case_mode is "smart_case"
    }
  }
 }
}

-- To get fzf loaded and working with telescope, you need to call
-- load_extension, somewhere after setup function:
require('telescope').load_extension('fzf')
require('telescope').load_extension('projects')


-- Config project
-- Plugins ahmedkhalf/project.nvim
require("project_nvim").setup {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }


-- Config Lualine
-- Plugins shadmansaleh/lualine.nvim 
require'lualine'.setup {

}


-- Config toggleterm
-- Plugins use akinsho/toggleterm.nvim
require("toggleterm").setup{}


-- Config Treesitter
-- Plugins nvim-treesitter/nvim-treesitter
require'nvim-treesitter.configs'.setup {}





