
local present, packer = pcall(require, "packerInit")
if present then
    packer = require "packer"
else
    return false
end



--[[
--Packer compiles plugins check which is comiling between packer or impatient
vim.cmd([[
augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
augroup end
)
]]--



local use = packer.use
return packer.startup(function()
    use { "wbthomason/packer.nvim",
        event = "VimEnter",
    }

    use { 'lewis6991/impatient.nvim',
        event = 'VimEnter'
    }

    -- Needed while issue https://github.com/neovim/neovim/issues/12587 is still open
    use { "antoinemadec/FixCursorHold.nvim" }


    use { "nvim-lua/plenary.nvim" }

   -- change theme to your own
    use { "HybridSquirrel/Oakleaves-Forest-Theme",
        after = "packer.nvim",
        config = function()
            require('forest').set()
         --vim.cmd [[colorscheme nord]]
        end,
    }

    use { "kyazdani42/nvim-web-devicons",
        after = "Oakleaves-Forest-Theme",
    }

    -- configure galaxyline
    use { "NTBBloodbath/galaxyline.nvim",
        after = "nvim-web-devicons",
        config = function()
            require "plugins.statusline"
        end,
    }

    use { "lukas-reineke/indent-blankline.nvim",
        after = "Oakleaves-Forest-Theme",
        config = function()
            require("plugins.others").blankline()
        end,
    }

    use { "norcalli/nvim-colorizer.lua",
        after = "Oakleaves-Forest-Theme",
        cmd = 'ColorizerToggle',
        config = function()
            require("plugins.others").colorizer()
        end,
    }

    use { "nvim-treesitter/nvim-treesitter",
        after = "Oakleaves-Forest-Theme",
        config = function()
            require "plugins.treesitter"
        end,
    }

   -- make a deeper check
    use { "nvim-treesitter/playground",
        cmd = 'TSPlayground',
    }

    use { "neovim/nvim-lspconfig",
        opt = true,
        setup = function()
            require("options").packer_lazy_load "nvim-lspconfig"
                vim.defer_fn(function()
                vim.cmd 'if &ft == "packer" | echo "" | else | silent! e %'
            end, 0)
        end,
        config = function()
            require "plugins.lspconfig"
        end,
    }

    use { "williamboman/nvim-lsp-installer",
        after = "nvim-lspconfig",
    }

    use { "rafamadriz/friendly-snippets",
        event = "InsertEnter",
    }

    use { "hrsh7th/nvim-cmp",
        after = "friendly-snippets",
        config = function()
            require "plugins.cmp"
        end,
    }

    use { "L3MON4D3/LuaSnip",
        wants = "friendly-snippets",
        after = "nvim-cmp",
        config = function()
            require("plugins.others").luasnip()
        end,
    }

    use { "saadparwaiz1/cmp_luasnip",
        after = "LuaSnip",
    }

    use { "hrsh7th/cmp-nvim-lua",
        after = "cmp_luasnip",
    }

    use { "hrsh7th/cmp-nvim-lsp",
        after = "cmp-nvim-lua",
    }

    use { "hrsh7th/cmp-calc",
        after = "nvim-cmp",
    }

    use { "hrsh7th/cmp-cmdline",
        after = "nvim-cmp",
    }

    -- wtf is this plugin, maybe remove, add others 
    use { "lukas-reineke/cmp-rg",
        after = "cmp-nvim-lsp",
    }

    use { "hrsh7th/cmp-path",
        after = "cmp-rg",
    }


    use { "windwp/nvim-autopairs",
        after = "nvim-cmp",
        config = function()
            require "plugins.autopairs"
        end
    }

    use { "kyazdani42/nvim-tree.lua",
        cmd = { "NvimTreeToggle", "NvimTreeFocus" },
        config = function()
            require "plugins.nvimtree"
        end,
    }
       
    
    use { "nvim-telescope/telescope.nvim",
        cmd = "Telescope",
        requires = {
            { "nvim-telescope/telescope-fzf-native.nvim", run = "make", },
        },
        config = function()
        require "plugins.telescope"
        end,
    }



    use { "folke/zen-mode.nvim",
        config = function()
            require "plugins.zenmode"
        end
    }

    
    use { "folke/twilight.nvim",
        after = "zen-mode",
        cmd = {
            "Twilight",
            "TwilightEnable",
        },
        config = function()
            require("twilight").setup {}
        end,
    }

    
    use { "akinsho/toggleterm.nvim",
                config = function()
            require "plugins.toggleterm"
        end
    }

    use { "glepnir/dashboard-nvim" }

    -- check if it really compiles from impatient
    -- Compiles the plugin file for caching
    --config = {
    --compile_path = vim.fn.stdpath('config')..'/lua/packer_compiled.lua'
    --}
    --Then make sure to add require('packer_compiled') somewhere in your config.
    --

    if packer_bootstrap then
        require('packer').sync()
    end
end)
