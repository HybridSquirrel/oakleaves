
-- Auto compile when file is changed
vim.cmd([[
	augroup packer_user_config
		autocmd!
		autocmd BufWritePost plugins.lua source <afile> | PackerComplie
	augroup end
]])


-- Imports plugins
require('packer').startup(function()
	
	-- Head Plugins
	use {'wbthomason/packer.nvim'}
	use {'lewis6991/impatient.nvim'}
	use {'antoinemadec/FixCursorHold.nvim'} -- Needed while issue https://github.com/neovim/neovim/issues/12587 is still open
	--use {'kyazdani42/nvim-web-devicons'}

	-- Addons Plugins 
	

	-- Ez of writing
	use {'windwp/nvim-autopairs'}

	use {'neovim/nvim-lspconfig'}	
	use {'williamboman/nvim-lsp-installer'}

	use {'hrsh7th/nvim-cmp'}	
	use {'hrsh7th/cmp-buffer'}
	use {'hrsh7th/cmp-nvim-lsp'}
	
	-- Project Managment
	use {'nvim-telescope/telescope.nvim',
	requires = { {'nvim-lua/plenary.nvim'}, 
	{'kyazdani42/nvim-web-devicons'}
	}
	}

	use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }	
	
	use {'ahmedkhalf/project.nvim'}	


	-- UI
	use {'shadmansaleh/lualine.nvim',
	requires = {'kyazdani42/nvim-web-devicons', opt = true}
	}
	use {'rinx/nvim-minimap'}
	use {"akinsho/toggleterm.nvim"}
	use {'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
	}
	use {'glepnir/dashboard-nvim'}


	-- Compiles the plugin file for caching
	config = {
		compile_path = vim.fn.stdpath('config')..'/lua/packer_compiled.lua'
	}
	
	
end)



